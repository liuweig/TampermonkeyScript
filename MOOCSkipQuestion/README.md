## 前言

### 支持作者

<span style="color:red;">如果你喜欢该脚本，可以打开下面的微信小程序支持一下作者。</span>  
![支持作者小程序码](https://greasyfork.org/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBekdIQVE9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--56691abdd507118966e2810dd47b1e2a3b9b82e8/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCem9MWm05eWJXRjBTU0lKYW5CbFp3WTZCa1ZVT2hSeVpYTnBlbVZmZEc5ZmJHbHRhWFJiQjJrQnlHa0J5QT09IiwiZXhwIjpudWxsLCJwdXIiOiJ2YXJpYXRpb24ifX0=--4c3cded9533f8c872a82572269844d930809aad4/support.png?locale=zh-CN)

### 所有脚本开源地址，欢迎 star ⭐

- gitee：[https://gitee.com/Kaiter-Plus/TampermonkeyScript](https://gitee.com/Kaiter-Plus/TampermonkeyScript)
- github：[https://github.com/Kaiter-Plus/TampermonkeyScript](https://github.com/Kaiter-Plus/TampermonkeyScript)

# <span style="color:red">!!注意!!</span>

- <span style="color:red">本脚本仅供学习使用，请勿用于非法用途！</span>
- <span style="color:red">使用本脚本用于非法用途后果自负！</span>
- <span style="color:red">请于下载后 24 小时内删除！</span>

## 功能介绍

- 中国大学 MOOC,icourse163 视频中题目自动跳过，移除弹题元素，防止暂停
